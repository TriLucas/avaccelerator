import math

from . import BaseGradient


class ExponentialGradient(BaseGradient):
    name = 'exponential'

    def _square_compression_vertical(self, initial_value, target_value, steps):
        return (target_value - initial_value) / math.pow(steps, 2)

    def _point_mirror(self, obj_p, ref):
        _con = ref[0] - obj_p[0], ref[1] - obj_p[1]
        return ref[0] + _con[0], ref[1] + _con[1]

    def _fnc_exp(self, initial_value, target_value, steps):
        compression = self._square_compression_vertical(initial_value,
                                                        target_value, steps)

        def _fnc(x):
            return initial_value + compression * math.pow(x, 2)
        return _fnc

    def _gradient_fnc(self, initial_value, target_value, steps):
        steps -= 1
        compression = self._square_compression_vertical(initial_value,
                                                        target_value, steps)

        def _fnc_normal(x):
            return initial_value + compression * math.pow(x, 2)

        # Apply mirroring when necessary
        if target_value < initial_value:
            mid_y = initial_value - (initial_value - target_value) / 2
            mid_x = steps / 2

            def _fnc_mirrored(x):
                value = _fnc_normal(steps - x)
                return self._point_mirror(
                    (x, value),
                    (mid_x, mid_y),
                )[1]
            return _fnc_mirrored

        return _fnc_normal


obj = ExponentialGradient()
