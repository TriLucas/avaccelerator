from utils import single_line


class BaseGradient:
    def __init__(self, *args, **kwargs):
        self._factory = None
        super().__init__(*args, **kwargs)

    def postfix(self, initial, target, steps):
        return f'_{initial:.2f}-{target:.2f}-{steps:.2f}_{self.name}'

    def _validate_params(self, **kwargs):
        for param_name in kwargs:
            value = kwargs[param_name]
            if value < 0:
                raise self.NegativeParamError(param_name=param_name,
                                              value=value)
            if value == 0:
                raise self.ZeroValueError(param_name=param_name,
                                          value=value)

    def validate_params(self, initial, target, steps):
        self._validate_params(start_speed=initial, end_speed=target,
                              resolution=steps)

    def __call__(self, initial, target, steps):
        if initial == target or steps == 0:
            raise self.NotAGradientError('Void gradient specified.')

        fnc = self._gradient_fnc(initial, target, steps)

        for step in range(steps):
            yield fnc(step)

    def register_with(self, gradient_factory):
        self._factory = gradient_factory
        gradient_factory.register(self.name, self)

    class NotAGradientError(Exception):
        error_code = 22

    class InvalidParameterError(Exception):
        error_code = 23

        def __init__(self, param_name, value, *args, **kwargs):
            self._param_name = param_name
            self._value = value
            super().__init__(*args, **kwargs)

        def __str__(self):
            _msg = f'''
            The value for {self._param_name} was invalid! Reason:
            {self._reason} ({self._value})
            '''
            return single_line(_msg)

        def __unicode__(self):
            return self.__str__()

    class ZeroValueError(InvalidParameterError):
        error_code = 24
        _reason = 'Value can not be Zero'

    class NegativeParamError(InvalidParameterError):
        error_code = 25
        _reason = 'Value can not be negative'
