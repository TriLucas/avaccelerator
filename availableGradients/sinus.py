import math

from . import BaseGradient


class SinusBellGradient(BaseGradient):
    name = 'sinus'

    def _gradient_fnc(self, initial_value, target_value, steps):
        _step_val = math.pi / (steps - 1)
        _bell_range = target_value - initial_value

        def fnc(x):
            _fac = math.sin(_step_val * x)
            return initial_value + _bell_range * _fac

        return fnc


obj = SinusBellGradient()
