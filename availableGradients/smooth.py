from .exponential import ExponentialGradient


class SmoothGradient(ExponentialGradient):
    name = 'smooth'

    def _gradient_fnc(self, initial_value, target_value, steps):
        mid_y = ((target_value - initial_value) / 2) + initial_value
        mid_x = (steps - 1) / 2

        fnc_left = self._fnc_exp(initial_value, mid_y, mid_x)

        def _fnc(x):
            abs_x = mid_x - abs(mid_x - x)
            _left = fnc_left(abs_x)
            if x <= mid_x:
                return _left
            return self._point_mirror((abs_x, _left), (mid_x, mid_y))[1]

        return _fnc


obj = SmoothGradient()
