from . import BaseGradient


class LinearGradient(BaseGradient):
    name = 'linear'

    def _gradient_fnc(self, initial_value, target_value, steps):
        # f(x) = m * x + b
        inclination = (target_value - initial_value) / (steps - 1)

        def _fnc(x):
            return initial_value + inclination * x

        return _fnc


obj = LinearGradient()
