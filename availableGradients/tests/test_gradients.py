import pytest

from gradients import factory
from availableGradients import BaseGradient


def lists_of_floats_are_equal(a, b):
    _set_a = set([f'{item:.3f}' for item in a])
    _set_b = set([f'{item:.3f}' for item in b])

    assert len(_set_a) == len(_set_b)

    for pair in zip(_set_a, _set_b):
        assert pair[0] == pair[1]


@pytest.mark.parametrize('gradient,start,end,steps', [
    ('linear', 5, 0, 0),
    ('exponential', 5, 0, 0),
    ('smooth', 5, 0, 0),
    ('sinus', 5, 0, 0),
])
def test_gradients_errors(gradient, start, end, steps):
    with pytest.raises(BaseGradient.NotAGradientError):
        list(factory.get_gradient(gradient)(start, end, steps))


def _test_gradient(gradient, start, end, steps, expected):
    generator = factory.get_gradient(gradient)(start, end, steps)

    data = list(generator)
    assert len(data) == steps
    lists_of_floats_are_equal(data, expected)
    assert start in data
    assert end in data


linear_data = [
    ('linear', 0, 5, 5, [0., 1.25, 2.5, 3.75, 5.]),
    ('linear', 0, 5, 6, [0., 1., 2., 3., 4., 5.]),
    ('linear', 5, 0, 5, [5., 3.75, 2.5, 1.25, 0.]),
    ('linear', -3, 3, 6, [-3., -1.8, -0.6, .6, 1.8, 3.]),
]


@pytest.mark.parametrize('gradient,start,end,steps,expected', linear_data)
def test_gradients_linear(gradient, start, end, steps, expected):
    _test_gradient(gradient, start, end, steps, expected)


exp_data = [
    ('exponential', 0, 5, 5, [0., .3125, 1.25, 2.8125, 5.]),
    ('exponential', 0, 5, 6, [0., .2, .8, 1.8, 3.2, 5.]),
    ('exponential', 5, 0, 5, [5., 2.8125, 1.25, .3125, 0.]),
    ('exponential', -3, 3, 6, [-3., -2.76, -2.04, -0.84, .84, 3.]),
]


@pytest.mark.parametrize('gradient,start,end,steps,expected', exp_data)
def test_gradients_exponential(gradient, start, end, steps, expected):
    _test_gradient(gradient, start, end, steps, expected)


exp_data = [
    ('smooth', 0, 5, 5, [0., .625, 2.5, 4.375, 5.]),
    ('smooth', 0, 5, 6, [0., .4, 1.6, 3.4, 4.6, 5.]),
    ('smooth', -3, 3, 6, [-3., -2.52, -1.08, 1.08, 2.52, 3.]),
    ('smooth', 5, 0, 5, [5., 4.375, 2.5, .625, 0.]),
]


@pytest.mark.parametrize('gradient,start,end,steps,expected', exp_data)
def test_gradients_smooth(gradient, start, end, steps, expected):
    _test_gradient(gradient, start, end, steps, expected)


exp_data = [
    ('sinus', 0, 5, 5, [0., 3.53553390, 5., 3.53553390, 0.]),
    ('sinus', 0, 5, 6, [0., 2.9389, 4.7553, 4.7553, 2.9389, 0.]),
    ('sinus', -3, 3, 6, [-3., 0.5267, 2.7063, 2.7063, 0.5267, -3.]),
    ('sinus', 5, 0, 5, [5., 1.4645, 0., 1.4645, 5.]),
]


@pytest.mark.parametrize('gradient,start,end,steps,expected', exp_data)
def test_gradients_sinus(gradient, start, end, steps, expected):
    generator = factory.get_gradient(gradient)(start, end, steps)
    data = list(generator)

    assert len(data) == len(expected)

    for pair in zip(data, expected):
        assert f'{pair[0]:.4f}' == f'{pair[1]:.4f}'

    assert len(data) == steps
