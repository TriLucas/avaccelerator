import importlib
import os

GRADIENTS_PATH = 'availableGradients'

MODULE_BLACKLIST = [
    '__pycache__',
    'tests',
]


class GradientFactory:
    def __init__(self, *args, **kwargs):
        self._instances = {}
        super().__init__(*args, **kwargs)

    def get_gradient(self, name):
        return self._instances[name]

    def register(self, name, instance):
        self._instances[name] = instance

    def get_available(self):
        return self._instances.keys()

    def discover(self):
        base_dir = os.path.dirname(__file__)

        modules = os.walk(os.path.join(base_dir, GRADIENTS_PATH))

        for cur_dir, sub_folders, files in modules:
            folder = os.path.basename(cur_dir)
            if folder in MODULE_BLACKLIST:
                continue

            for source_file in [f for f in files if f != '__init__.py']:
                package = os.path.splitext(source_file)[0]
                mod = importlib.import_module('.'.join([folder, package]))

                mod.obj.register_with(self)


factory = GradientFactory()
factory.discover()
