from moviepy.editor import VideoClip
from moviepy.editor import vfx

from logs import logger


class AcceleratedChunk:
    def __init__(self, input_clip, speed, n_chunks=0):
        self._input_clip = input_clip
        self._n_chunks = n_chunks

        self._base_duration = input_clip.duration / n_chunks

        self.speed = speed

        self._input_start = None
        self._input_end = None

        self._output_start = None
        self._output_end = None

        self._accelerated = None

    @property
    def accelerated(self):
        if self._accelerated is None:
            _msg = f'''
            Creating truncated subclip from
            {self._input_start}:{self._input_end} to
            {self._output_start}:{self._output_end}
            '''
            logger.debug(_msg)
            self._accelerated = self._input_clip.subclip(self._input_start)
            self._accelerated = self._accelerated.fx(vfx.speedx, self.speed)
        return self._accelerated

    def get_frame(self, out_time):
        _offset = out_time - self._output_start
        _rel_offset = _offset * self.speed
        logger.debug(f'Offset to input start = {_rel_offset}')
        return self._input_clip.get_frame(self._input_start + _rel_offset)

    @property
    def duration(self):
        return self._base_duration / self.speed

    def set_input_boundaries(self, idx):
        self._input_start = self._base_duration * idx
        self._input_end = self._input_start + self._base_duration

    def set_output_boundaries(self, start):
        self._output_start = start
        self._output_end = start + self.duration

    def handles_frame_position(self, output_time):
        return self._output_start <= output_time < self._output_end

    def close(self):
        self._input_clip.close()
        self._input_clip = None


class ChunkList:
    def __init__(self, chunks, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._chunks = chunks

    @classmethod
    def from_speeds(cls, speeds, input_clip):
        _out_start = 0
        _chunks = []
        for idx, speed in enumerate(speeds):
            _chunk = AcceleratedChunk(input_clip, speed, len(speeds))
            _chunk.set_input_boundaries(idx)
            _chunk.set_output_boundaries(_out_start)
            _out_start += _chunk.duration

            _chunks.append(_chunk)

        obj = cls(_chunks)
        return obj

    @property
    def duration(self):
        return sum(chunk.duration for chunk in self._chunks)

    def for_out_time(self, out_time):
        for chunk in self._chunks:
            if chunk.handles_frame_position(out_time):
                return chunk

    def close(self):
        for chunk in self._chunks:
            chunk.close()


class VideoClipGradientAcceleration:
    def create_writer(self):
        def make_frame(out_time):
            chunk = self._chunks.for_out_time(out_time)

            return chunk.get_frame(out_time)

        writer = VideoClip(make_frame=make_frame).set_fps(self.fps)
        writer = writer.set_duration(self.duration)
        return writer.fx(vfx.speedx, 1 / self._input_stretch)

    def __init__(self, input_clip, chunk_speeds, normalization_factor=1,
                 input_dilation_factor=1, *args, **kwargs):

        self._input_stretch = normalization_factor * input_dilation_factor

        self._input_clip = input_clip.fx(vfx.speedx, self._input_stretch)

        self._chunks = ChunkList.from_speeds(chunk_speeds, self._input_clip)

        self._fps = None

        super().__init__(*args, **kwargs)

        self._out_values = {}

    @property
    def fps(self):
        return self._fps or self._input_clip.fps

    @property
    def duration(self):
        return self._chunks.duration

    def close(self):
        self._chunks.close()
        self._chunks = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
        if exc_type is not None:
            raise exc_type(exc_val)
