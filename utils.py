import multiprocessing

from datetime import datetime
from inspect import cleandoc
from functools import wraps
from functools import partial
from textwrap import dedent

import logging

logger = logging.getLogger('AVAccelerator')

RE_RAISE_ERRORS = False


def single_line(msg):
    return ' '.join(dedent(cleandoc(msg)).split())


def graceful_process(fnc=None, exceptions=(), re_raise=False):
    if not fnc:
        return partial(graceful_process, exceptions=exceptions)

    @wraps(fnc)
    def wrapper(*args, **kwargs):
        try:
            return fnc(*args, **kwargs)
        except exceptions as e:
            logger.error(e)
            if not RE_RAISE_ERRORS:
                return e.error_code, str(e)
            raise e
        return 1, None
    return wrapper


def timed(fnc=None, level='INFO'):
    if not fnc:
        return partial(timed, level=level)

    def log_fnc(*args, **kwargs):
        logger.log(logging.getLevelName(level.upper()), *args, **kwargs)

    @wraps(fnc)
    def wrapper(*args, **kwargs):
        start = datetime.now()

        result = fnc(*args, **kwargs)

        end = datetime.now()
        log_fnc(f'Finished "{fnc.__name__}" after {end - start}')
        return result
    return wrapper


@timed(level='INFO')
def write_clip_composition(writer, out_file, codec=None):
    _logger = None
    if logger.level <= logging.INFO:
        _logger = 'bar'
    writer.write_videofile(
        out_file,
        codec=codec,
        logger=_logger,
        threads=max([multiprocessing.cpu_count() - 1, 1]),
        ffmpeg_params=['-crf', '15'],
        preset='slow',
    )
