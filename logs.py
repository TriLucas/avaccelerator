import logging


logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s',
                    level=logging.CRITICAL)
logger = logging.getLogger('AVAccelerator')
logger.setLevel(logging.NOTSET)


def set_level(log_level):
    logger.setLevel(logging.getLevelName(log_level))


def get_log_levels():
    return [
        'CRITICAL',
        'ERROR',
        'WARNING',
        'INFO',
        'DEBUG',
        'NOTSET',
    ]
