# AVAccelerator

Simple command line to for slowing or accelerating videos.

### Background

I frequently face the objective to gradually accelerate and / or decelerate a
video over a defined period of time. Not every video editing software provides
the means to do this, usually the possibilities are limited to a fixed playback
time per clip (like in e.g. [Shotcut][1]).

AVAccelerator was created in order to have a lightweight and simple tool for
manipulating a video's playtime along a configurable gradient.

### Speed gradients

The simplest approach for changing a video's playback time would be to set its
general playback time to a fixed value, but as mentioned above this is fairly
inflexible and doesn't allow for e.g. smooth, cineastic slowmotion exits or
entrances.

The idea of AVAccelerator is to split the input into chunks and modify each
chunk's playback time by a different acceleration factor (i.e. compressing /
stretching the chunks to different durations).

The acceleration factors can be described by equations, calculating the
factor according to a chunks position in the input video.

Please find more information about the available equations / gradients in
[available gradients](#available-gradients).

---

## Installation
### Requirements

You need the following software preinstalled on your machine prior to using
AVAccelerator:

* [Python >= 3.6][2]

Optional:

* [tox][3]

### Dependencies

#### Via pip

A `requirements.txt` file is provided for automatically fetching necessary
packages (preferably from within a virtualenv). Install these packages via:

    $ pip install -r requirements.txt

An additional dependencies file is provided for a development environment:

    $ pip install -r dev-requirements.txt

#### Via tox

For convenience, a tox environment is provided for automatically generating
virtual development environments with all dependencies installed:

    $ python -m tox -e venv-py{36,37,38,39}

To activate the virtual environment:

    # On Linux
    $ source venv-py<version>/bin/activate

    # On Windows
    venv-py<version>\Scripts\activate.bat

## Usage

AVAccelerator is separated into two functionalities, **accelerating** a single
video file and **resizing** one or more video files. These functionalities are
accessed via subcommands (see below). You should invoke AVAccelerator on the
commandline in the following form: `$ avaccelerator <global_params>
{accelerate|resize} <subcommand_params>`. The global parameters are:

 * `-i / --input-path`: the file (or folder) to process, **required**
 * `-o / --output-path`: the file (or folder) to write the results to
 * `-l / --log-level`: the verbosity of Python's logging facility

### accelerate

Below, please find two examples for using AVAccelerator's acceleration
command:

**Gradually decrease the playback time of input.mp4 in a linear fashion over 10
chunks, starting with 1.0x and ending with 0.2x speed:**

    $ avaccelerator --input-path input.mp4 \
                    accelerate \
                    --start-speed 1.0 \
                    --end-speed 0.2 \
                    --gradient-type linear \
                    --chunk-resolution 10

**Skip the first 10 seconds of input.mp4 and accelerate everything afterwards
exponentially, save the result as a .webm file**:

    $ avaccelerator --input-path input.mp4 \
                    accelerate \
                    --start-speed 1.0 \
                    --end-speed 10.0 \
                    --chunk-resolution 10\
                    --custom-extension webm \
                    --codec libvpx \
                    --gradient-type exponential

**For a full list of parameters and flags please refer to:**

    $ avaccelerator accelerate -h

### resize

Below, please find two examples for using AVAccelerator's resizing command:

**Resize every video in 'C:\Videos' to half its size**:

    $ avaccelerator --input-path C:\videos \
                    resize \
                    --scaling 0.5

**Resize every video in 'C:\Videos' to a specific resolution, written to
'H:\resized'**:

    $ avaccelerator --input-path C:\videos \
                    --output-path H:\resized
                    resize \
                    --width 640 \
                    --height 480

**For a full list of parameters and flags please refer to:**

    $ avaccelerator resize -h

---

## Available gradients for acceleration
### Linear

The `linear` gradient simply calculates the acceleration value via a line
equation, i.e. `f(x) = mx+b`.

![linear gradient](doc/linear.png)

| input | output |
|---|---|
|![original gif](doc/gif/original.gif)|![linear gif](doc/gif/linear.gif)|

### Exponential

The `exponential` gradient calculates the acceleration value as the second power
of the chunks position, with additionally applying compression or stretching to
the curve; i.e. `f(x) = mx² + b`

![exponential gradient](doc/exponential.png)

| input | output |
|---|---|
|![original gif](doc/gif/original.gif)|![exponential gif](doc/gif/expo.gif)|

### Smooth

The `smooth` gradient follows an s-shaped curve from start to end (while
mathematically a hack, internally this is done by taking a half
**Exponential gradient** and mirroring it)

![smooth gradient](doc/smooth.png)

| input | output |
|---|---|
|![original gif](doc/gif/original.gif)|![smooth gif](doc/gif/linear.gif)|

### Sinus

The `sinus` gradient projects the provided parameters onto the first half of a
normal sinus wave.

![sinus gradient](doc/sinus.png)

| input | output |
|---|---|
|![original gif](doc/gif/original.gif)|![smooth gif](doc/gif/sinus.gif)|

---

Please note that the provided values for `start` and `end` are always included
in the gradient, except for `sinus` when run with an even amount of chunks.

## Note about FFMPEG

AVAccelerator uses the python package [MoviePy][4], which among other things
provides an interface to `ffmpeg` executables. Please consult their [official
documentation][5] for information about codecs, codec parameters etc.

[1]: https://shotcut.org/
[2]: https://www.python.org/
[3]: https://tox.readthedocs.io/en/latest/
[4]: https://zulko.github.io/moviepy/
[5]: https://zulko.github.io/moviepy/ref/ffmpeg.html
