import math
import os

from moviepy.editor import VideoFileClip
from moviepy.tools import find_extension

from gradients import factory
from availableGradients import BaseGradient

from clip import VideoClipGradientAcceleration

from utils import graceful_process
from utils import single_line
from utils import timed
from utils import write_clip_composition

import logging

logger = logging.getLogger('AVAccelerator')

MIN_SUFFICIENT_FRAMES = 10


def outfile_name(input_path, post_fix='', codec=None, custom_extension=None):
    try:
        custom_extension = custom_extension or find_extension(codec)
    except ValueError:
        pass
    basefile, extension = os.path.splitext(input_path)

    out_base = basefile + post_fix
    out_extension = custom_extension or extension
    if not out_extension.startswith('.'):
        out_extension = f'.{out_extension}'

    return f'{out_base}{out_extension}'


def _stretch_to_multiple(total_frames, divider):
    current_frames = total_frames
    factor = 1
    while current_frames % divider != 0:
        current_frames += 1
        factor = total_frames / current_frames

    return round(current_frames / divider), factor


def threshold_satisfaction(fps, duration, start_speed, end_speed,
                           resolution, frames, frames_min_threshold):
    input_frames = round(duration * fps)

    portion, stretch = _stretch_to_multiple(input_frames, resolution or frames)
    logger.debug(f'Multiple fix: Stretching by {stretch:.3f}.')

    if resolution is not None:
        res_frames = portion
        res_resolution = resolution
    else:
        res_frames = frames
        res_resolution = portion

    min_frames = min([res_frames / start_speed, res_frames / end_speed])
    logger.debug(f'Minimal frames after acceleration: {min_frames}')

    if min_frames < frames_min_threshold:
        stretch_threshold = math.ceil(frames_min_threshold / min_frames)
        logger.info(f'Fix threshold violation: stretch by {stretch_threshold}')
        stretch /= stretch_threshold
        if resolution is not None:
            res_frames = round(res_frames * stretch_threshold)
        else:
            res_resolution = round(res_resolution * stretch_threshold)

    return stretch, res_resolution, res_frames


def quantify_duration(fps, duration):
    _shortest = 1 / fps
    _next_full = duration + (duration % _shortest)

    return duration / _next_full


@timed(level='DEBUG')
@graceful_process(exceptions=(BaseGradient.NotAGradientError,))
def process_ava(input_path, in_clip, start_speed=1, end_speed=1,
                gradient_type=None, output_path=None, codec=None,
                custom_extension=None, chunk_resolution=None,
                chunk_frames=None, min_chunk_frames=MIN_SUFFICIENT_FRAMES):
    gradient = factory.get_gradient(gradient_type)
    gradient.validate_params(
        start_speed,
        end_speed,
        chunk_resolution if chunk_resolution is not None else chunk_frames,
    )

    normalize_speed = quantify_duration(in_clip.fps, in_clip.duration)

    _msg = f'''
    Normalizing to  {in_clip.duration / normalize_speed * in_clip.fps} frames
    with factor {normalize_speed:.3f}.
    '''
    logger.info(single_line(_msg))

    input_speed, input_resolution, input_frames = threshold_satisfaction(
        in_clip.fps,
        in_clip.duration / normalize_speed,
        start_speed,
        end_speed,
        chunk_resolution,
        chunk_frames,
        min_chunk_frames,
    )

    _msg = f'''
    Stretching input by {input_speed:.3f} to get {input_resolution} chunks @
    {input_frames} frames.
    '''
    logger.info(single_line(_msg))

    speeds = gradient(start_speed, end_speed, input_resolution)

    vcga_args = in_clip, list(speeds), normalize_speed, input_speed
    with VideoClipGradientAcceleration(*vcga_args) as vcga:
        out_postfix = gradient.postfix(start_speed, end_speed,
                                       input_resolution)
        out_file = output_path or outfile_name(input_path, out_postfix, codec,
                                               custom_extension)
        write_clip_composition(vcga.create_writer(), out_file, codec)

    return 0, f'Successfully manipulated {input_path} to {out_file}'


def avaccelerate(input_path, cut_start=0, cut_end=None, **kwargs):
    with VideoFileClip(input_path).subclip(cut_start, cut_end) as in_clip:
        return process_ava(input_path, in_clip, **kwargs)
