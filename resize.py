import os
import logging

from moviepy.editor import VideoFileClip

from utils import write_clip_composition
from utils import single_line


logger = logging.getLogger('AVAccelerator')


def apply(clip, width, height, scaling):
    if width is not None and height is not None:
        return clip.resize((width, height))
    if scaling is not None:
        return clip.resize(scaling)

    single = {key: value for key, value in
              [('height', height), ('width', width)]
              if value is not None}
    if len(single.keys()) == 1:
        return clip.resize(**single)
    raise Exception('Invalid parameters!')


def resized_filename(width, height, output_path):
    abs_dir = os.path.dirname(output_path)
    filename = os.path.basename(output_path)
    filename, extension = os.path.splitext(filename)

    return os.path.join(abs_dir,
                        f'{filename}_{width}x{height}{extension}')


def _resize_video(input_path, output_path, width, height, scaling):
    with VideoFileClip(input_path) as clip:
        clip = apply(clip, width, height, scaling)
        width, height = clip.size

        actual_output_path = resized_filename(width, height, output_path)
        if not os.path.exists(actual_output_path):
            write_clip_composition(clip, actual_output_path)
        else:
            _msg = f'''
            Skipping {input_path}, output file {actual_output_path} exists.
            '''
            logger.warning(single_line(_msg))


def resize_video(input_path, output_path=None, width=None, height=None,
                 scaling=None):
    if os.path.isfile(input_path):
        _resize_video(input_path, output_path, width, height, scaling)
    else:
        for cur_dir, folders, files in os.walk(input_path):
            _files = []
            if os.path.isabs(output_path):
                out_folder = output_path
            else:
                out_folder = os.path.join(cur_dir, output_path)
            if not os.path.exists(out_folder):
                os.mkdir(out_folder)
            for filename in files:
                _files.append(filename)
                out_file = os.path.join(out_folder, filename)

                in_file = os.path.join(cur_dir, filename)
                logger.info(f'Resizing {in_file} to {out_file}.')
                _resize_video(in_file, out_file, width, height, scaling)
            return 0, f'Resized {", ".join(_files)}.'
