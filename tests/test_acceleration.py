import pytest
import os

import random

from moviepy.editor import VideoFileClip

from accelerate import avaccelerate
from accelerate import threshold_satisfaction
from accelerate import quantify_duration

from availableGradients import BaseGradient

from conftest import file_md5


@pytest.mark.parametrize('fps,duration,expected_speed', [
    (10, .25, .83333),
    (30, 27.187, .99925),
])
def test_acceleration_duration_fps_quantified(fps, duration, expected_speed):
    assert f'{expected_speed:.5f}' == f'{quantify_duration(fps, duration):.5f}'


@pytest.mark.parametrize(
    'fps,resolution,duration,start,end',
    [[random.randint(10, 100) for x in range(5)] for y in range(100)],
)
def test_acceleration_stretch_transitivity_100(fps, resolution, duration,
                                               start, end):
    _normalize_fac = quantify_duration(fps, duration)
    duration /= _normalize_fac

    _n_frames = fps * duration

    frames = round(_n_frames / resolution)

    speed_a, res_a, frames_a = threshold_satisfaction(
        fps, duration, start, end, resolution, None, frames + 1
    )

    speed_b, res_b, frames_b = threshold_satisfaction(
        fps, duration, start, end, None, frames, frames + 1
    )

    duration_a = ((res_a * frames_a) * speed_a) / fps
    duration_b = ((res_b * frames_b) * speed_b) / fps

    assert f'{duration_a:.5f}' == f'{duration_b:.5f}'
    assert f'{duration_a:.5f}' == f'{duration:.5f}'


@pytest.mark.parametrize(
    'start,end,chunks,resolution,duration,exc_class,exc_match',
    [
        (-1.5, .5, None, 500, 11., BaseGradient.NegativeParamError,
         r'.*start_speed.*-1.5.*'),
        (1.5, -.5, None, 500, 11., BaseGradient.NegativeParamError,
         r'.*end_speed.*-0.5.*'),
        (1.5, .5, None, -5, 11., BaseGradient.NegativeParamError,
         r'.*resolution.*-5.*'),
        (.50, .5, -5, None, 11., BaseGradient.NegativeParamError,
         r'.*resolution.*-5.*'),
        (0, .5, None, 500, 11., BaseGradient.ZeroValueError,
         r'.*start_speed.*'),
        (.5, 0, None, 500, 11., BaseGradient.ZeroValueError,
         r'.*end_speed.*'),
        (1.5, .5, None, 0, 11., BaseGradient.ZeroValueError,
         r'.*resolution.*'),
        (.5, .5, 0, None, 11., BaseGradient.ZeroValueError,
         r'.*resolution.*'),
    ]
)
def test_acceleration_input_sanity(simple_random_clip, simple_output_path,
                                   start, end, chunks, resolution, duration,
                                   exc_class, exc_match):
    with pytest.raises(exc_class, match=exc_match):
        avaccelerate(simple_random_clip, start_speed=start, end_speed=end,
                     output_path=simple_output_path, gradient_type='linear',
                     chunk_frames=chunks, chunk_resolution=resolution)


avacc_inputs = 'start,end,chunks,resolution,duration'
avacc_params = [
    (1.5, .5, None, 500, 11.),
    (1.5, .5, 3, None, 11.),
    (.5, 2.5, None, 500, 8.07),
    (.5, 2.5, 3, None, 8.07),
    (1.5, .5, None, 10, 11.27),
    (1.5, .5, 50, None, 11.5),
    (.5, 2.5, None, 10, 8.53),
    (.5, 2.5, 50, None, 8.93),
]


@pytest.mark.parametrize(avacc_inputs, avacc_params)
def test_acceleration_processing(simple_random_clip, simple_output_path, start,
                                 end, chunks, resolution, duration):
    avaccelerate(simple_random_clip, start_speed=start, end_speed=end,
                 output_path=simple_output_path, gradient_type='linear',
                 chunk_frames=chunks, chunk_resolution=resolution)

    assert os.path.exists(simple_output_path)
    assert os.path.isfile(simple_output_path)

    with VideoFileClip(simple_output_path) as clip:
        assert clip.duration == duration


@pytest.mark.parametrize(avacc_inputs, avacc_params)
def test_acceleration_idempotency(simple_random_clip, output_data_dir, start,
                                  end, chunks, resolution, duration):
    file1 = str(output_data_dir.join('md5_1.mp4'))
    file2 = str(output_data_dir.join('md5_2.mp4'))

    avaccelerate(simple_random_clip, start_speed=start, end_speed=end,
                 output_path=file1, gradient_type='linear',
                 chunk_frames=chunks, chunk_resolution=resolution)

    avaccelerate(simple_random_clip, start_speed=start, end_speed=end,
                 output_path=file2, gradient_type='linear',
                 chunk_frames=chunks, chunk_resolution=resolution)

    assert file_md5(file1) == file_md5(file2)
