import os

import pytest
from moviepy.editor import VideoFileClip

from resize import resize_video
from resize import resized_filename

from conftest import file_md5


@pytest.mark.parametrize('width,height,scaling,out_width,out_height', [
    (None, None, .5, 310, 240),
    (None, None, .25, 155, 120),
    (310, 240, None, 310, 240),
    (310, 100, None, 310, 100),
    (100, 310, None, 100, 310),
])
def test_resize_creation(input_data_dir, simple_random_clip,
                         another_random_clip, width, height, scaling,
                         out_height, out_width):
    filenames = [os.path.basename(simple_random_clip),
                 os.path.basename(another_random_clip)]

    folder = os.path.dirname(simple_random_clip)
    resize_video(folder, 'test_resized', width=width, height=height,
                 scaling=scaling)

    assert os.path.exists(os.path.join(input_data_dir, 'test_resized'))

    for filename in filenames:
        original_name = os.path.join(input_data_dir, filename)
        abs_filename = os.path.join(input_data_dir, 'test_resized', filename)
        resized_name = resized_filename(out_width, out_height, abs_filename)

        assert os.path.exists(resized_name)
        assert os.path.isfile(resized_name)

        with VideoFileClip(resized_name) as resized_clip:
            with VideoFileClip(original_name) as original_clip:
                assert original_clip.duration == resized_clip.duration
                assert original_clip.fps == resized_clip.fps
                res_width, res_height = resized_clip.size

                assert res_width == out_width
                assert res_height == out_height


@pytest.mark.parametrize('width', [100, 200, 300])
@pytest.mark.parametrize('height', [100, 200, 300])
def test_resize_idempotency(input_data_dir, simple_random_clip,
                            another_random_clip, width, height):
    filenames = [os.path.basename(simple_random_clip),
                 os.path.basename(another_random_clip)]

    folder = os.path.dirname(simple_random_clip)
    resize_video(folder, 'test_resized', width=width, height=height)
    resize_video(folder, 'test_resized_vgl', width=width, height=height)

    for filename in filenames:
        vgl_name = os.path.join(input_data_dir, 'test_resized_vgl', filename)
        vgl_name = resized_filename(width, height, vgl_name)

        resized_name = os.path.join(input_data_dir, 'test_resized', filename)
        resized_name = resized_filename(width, height, resized_name)

        assert file_md5(resized_name) == file_md5(vgl_name)
