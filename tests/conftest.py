import hashlib
import random

from datetime import datetime

import numpy as np
import pytest

import utils
from moviepy.editor import ImageClip
from moviepy.editor import concatenate_videoclips


utils.RE_RAISE_ERRORS = True
random.seed(datetime.now().timestamp())


def file_md5(filepath):
    md5 = hashlib.md5()
    with open(filepath, 'rb') as fp:
        def read4096():
            return fp.read(4096)
        for chunk in iter(read4096, b''):
            md5.update(chunk)
    return md5.hexdigest()


@pytest.fixture(scope='session')
def input_data_dir(tmpdir_factory):
    return tmpdir_factory.mktemp('sources')


@pytest.fixture(scope='session')
def output_data_dir(tmpdir_factory):
    return tmpdir_factory.mktemp('output')


@pytest.fixture(scope='session')
def simple_output_path(output_data_dir):
    return str(output_data_dir.join('OUT.mp4'))


def make_random_clip(filename):
    r_clips = []
    for x in range(10):
        im_data = np.array(np.random.rand(480, 620, 3) * 255, dtype=np.uint8)
        r_clips.append(ImageClip(im_data, duration=1).set_fps(30))
    with concatenate_videoclips(r_clips).set_fps(30) as clip:
        utils.write_clip_composition(clip, filename)


@pytest.fixture(scope='session')
def simple_random_clip(input_data_dir):
    file_path = str(input_data_dir.join('simple.mp4'))
    make_random_clip(file_path)
    return file_path


@pytest.fixture(scope='session')
def another_random_clip(input_data_dir):
    file_path = str(input_data_dir.join('another.mp4'))
    make_random_clip(file_path)
    return file_path
